import Web3 from 'web3'

let web3

if (typeof window !== 'undefined' && typeof window.web3 !== 'undefined') {
  // Entramos aquí cuando estemos en el navegador y corriendo metamask
  window.ethereum.enable();
  web3 = new Web3(window.web3.currentProvider);
} else {
  // Entramos aquí cuando estamos en el servidor *O* el usuario no está usando metamask
  // Así es que creamos nuestro propio proveedor
  const provider = new Web3.providers.HttpProvider(
    'https://rinkeby.infura.io/v3/<CODIGO_INFURA>'
  );
  web3 = new Web3(provider);
}

export default web3
